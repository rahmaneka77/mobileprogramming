//No.1 (Range)
function range(startNum, finishNum) {
    var range = [];
	for (var i = startNum; i <= finishNum; i++){
		range.push(i);
	}
	for (var i = startNum; i >= finishNum; i--){
		range.push(i);
	}
	for (var u = finishNum; u == undefined; u++) {
		range.push(-1);
	}
	return range
}
console.log(range(1,10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())
console.log("")

//No.2 (Range with step)
function rangeWithStep(startNum, finishNum, step) {
    var rangeWithStep = [];
	for (var i = startNum; i <= finishNum; i+=step){
		rangeWithStep.push(i);
	}
	for (var i = startNum; i >= finishNum; i-=step){
		rangeWithStep.push(i);
	}
	return rangeWithStep
}
console.log(rangeWithStep(1,10,2))
console.log(rangeWithStep(11,23,3))
console.log(rangeWithStep(5,2,1))
console.log(rangeWithStep(29,2,4))
console.log("")

//No.3 (Sum of range)
function sum(startNum, finishNum, step) {
	var x = [];
	if (startNum == null) {
		return (0);
	}
	if (step >= 1) {
		for (var y = startNum; y <= finishNum; y += step) {
			x.push(y)
		}
	} else {
		for (var z = startNum; z >= finishNum; z += step) {
			x.push(z)
		}
	}
	if (step=="") {
		for (var h = startNum; h <= finishNum; h++) {
			x.push(h)
		}
	}
	return x.reduce ((a, b) => a + b, 1);
}
console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, -2));
console.log(sum(1));
console.log(sum());
console.log("")

//No.4 (Array multidimensi)
var input = [
["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989"," Membaca"],
["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
["0004", "Bintang Senjaya", "Martapura", "6/4/1970","Berkebun"]
]
console.log("Nomor id: "+input[0][0]+"\nNama Lengkap: "+input[0][1]+"\nTTL: "+input[0][2]+" "+input[0][3]+"\nHobi: "+input[0][4]) 
console.log("\nNomor id: "+input[1][0]+"\nNama Lengkap: "+input[1][1]+"\nTTL: "+input[1][2]+" "+input[1][3]+"\nHobi: "+input[1][4])
console.log("\nNomor id: "+input[2][0]+"\nNama Lengkap: "+input[2][1]+"\nTTL: "+input[2][2]+" "+input[2][3]+"\nHobi: "+input[2][4])
console.log("\nNomor id: "+input[3][0]+"\nNama Lengkap: "+input[3][1]+"\nTTL: "+input[3][2]+" "+input[3][3]+"\nHobi: "+input[3][4])
console.log("")

//No.5 (Balik kata)
function balikKata(str) {
	var currentString = str;
	var newString = '';
	for (let i = str.length - 1; i >= 0; i--) {
		newString = newString + currentString[i];
	}
	return newString;
}
console.log(balikKata('Kasur Rusak'))
console.log(balikKata('Informatika'))
console.log(balikKata('Haji Ijah'))
console.log(balikKata('racecar'))
console.log(balikKata('I am Humanika'))
console.log("")

//No.6 (Metode array)
var input = ["0001", "Roman Alamsyah Elsharawy", "Bandar Lampung", "21/5/1989", "membaca"];
var data1 = input[0]
var data2 = input[1]
var data3 = input[2]
var data4 = input[3]
var data5 = ('SMA Internasional Metro')
var alamat = data3.split(' ')
alamat.unshift('Provinsi')
var newalamat = alamat.join(" ")
var output = (data1+","+data2+","+newalamat+","+data4+",Pria,"+data5)
var newoutput = output.split(",")
console.log(newoutput)
var tanggal = data4.split('/')
var newtanggal = tanggal.join("-")
var bulan = tanggal[1] = 5
switch(bulan){
	case 1: {console.log('Januari'); break;}
	case 2: {console.log('Febuari'); break;}
	case 3: {console.log('Maret'); break;}
	case 4: {console.log('April'); break;}
	case 5: {console.log('Mei'); break;}
	case 6: {console.log('Juni'); break;}
	case 7: {console.log('Juli'); break;}
	case 8: {console.log('Agustus'); break;}
	case 9: {console.log('September'); break;}
	case 10: {console.log('Oktober'); break;}
	case 11: {console.log('November'); break;}
	case 12: {console.log('Desember'); break;}
default: {console.log('Tanggal tidak terdeteksi'); break;}}
var numbers = tanggal
numbers.sort()
var nama = data2.split(' ')
nama.pop()
var newnama = nama.join(" ")
console.log(numbers)
console.log(newtanggal)
console.log(newnama)