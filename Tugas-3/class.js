//No.1 (Animal Class)
class Animal {
	constructor(name, legs, cold_blooded) {
		this.name = name
		this.legs = 4
		this.cold_blooded = "False"
	}
	get name() {
		return this._name
	}
	set name(name) {
		this._name = name
	}
}
var sheep = new Animal("Shaun");
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded + "\n")

class Ape extends Animal{
	yell(){
		console.log(`${this.name} Auooo\n`)
	}
}
var sungokong = new Ape("Kera sakti")
sungokong.yell()

class Frog extends Animal{
	jump(){
		console.log(`${this.name} hop hop\n`)
	}
}
var kodok = new Frog("Buduk")
kodok.jump()

//No.2 (Function to Class)
class Clock {
	constructor({ template }) {
		this.template = template;
	}
	render() {
		let date = new Date();
		let hours = date.getHours();
		if (hours < 10) hours = '0' + hours;
		let mins = date.getMinutes();
		if (mins < 10) mins = '0' + mins;
		let secs = date.getSeconds();
		if (secs < 10) secs = '0' + secs;
		let output = this.template
		.replace('h', hours)
		.replace('m', mins)
		.replace('s', secs);
		console.log(output);
	}
	stop() {
		clearInterval(this.timer);
	}
	start() {
		this.render();
		this.timer = setInterval(() => this.render(), 1000);
	}
}
var clock = new Clock({template: 'h:m:s'});
clock.start();